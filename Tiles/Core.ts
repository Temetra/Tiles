/// <reference path="Interfaces.ts"/>

String.prototype.format = function() {
	var args = arguments;
	var format = function (match, number) {
		return typeof args[number] != 'undefined' ? args[number] : match;
	};
	return this.replace(/{(\d+)}/g, format);
};

window.requestAnimFrame = (function () {
	return window.requestAnimationFrame
		|| window.webkitRequestAnimationFrame
		|| window.mozRequestAnimationFrame
		|| window.oRequestAnimationFrame
		|| window.msRequestAnimationFrame
		|| function (callback, element) { window.setTimeout(callback, 1000 / 60); };
})();

Math.sign = function (i: number) {
	if (i < 0) return -1;
	return 1;
};

module _graphlib {
	export class Vector {
		// Init target with x and y
		static set(target: Vec2, x: number, y: number) {
			target.x = x;
			target.y = y;
		}

		// Copy source into target
		static copy(source: Vec2, target: Vec2) {
			target.x = source.x;
			target.y = source.y;
		}

		// Add left and right, set target
		static add(left: Vec2, right: Vec2, target: Vec2) {
			target.x = left.x + right.x;
			target.y = left.y + right.y;
		}

		// Subtract right from left, set target
		static subtract(left: Vec2, right: Vec2, target: Vec2) {
			target.x = left.x - right.x;
			target.y = left.y - right.y;
		}

		// Return copy of source
		static clone(source: Vec2): Vec2 {
			return { x: source.x, y: source.y };
		}

		// Returns true if left values === right values
		static equals(left: Vec2, right: Vec2): bool {
			return (left.x === right.x && left.y === right.y);
		}
	}
}