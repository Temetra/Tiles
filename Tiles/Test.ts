/// <reference path="Interfaces.ts"/>
/// <reference path="Core.ts"/>
/// <reference path="Tiles.ts"/>

module _test {
	var Vector = _graphlib.Vector;

	export var tiles = <_graphlib.Tiles>null;

	var moving = false;
	var lastMouse = { x: 0, y: 0 };
	var currentMouse = { x: 0, y: 0 };
	var delta = { x: 0, y: 0 };

	function mouseDown(e: MouseEvent) {
		moving = true;
	}

	function mouseUp(e: MouseEvent) {
		moving = false;
	}

	function mouseMove(e: MouseEvent) {
		Vector.copy(currentMouse, lastMouse);
		Vector.set(currentMouse, e.pageX, e.pageY);

		if (moving && tiles) {
			Vector.subtract(currentMouse, lastMouse, delta);
			var d = Vector.clone(delta);
			window.requestAnimFrame(function () { tiles.updateTiles(d); });
		}
	}
	
	function tileNeedsUpdate(tile) {
		//console.log("Tile needs update");
	}

	function onload(e: Event) {
		var viewport = document.getElementById("port");
		tiles = new _graphlib.Tiles(viewport, tileNeedsUpdate);
		viewport.addEventListener("mousedown", mouseDown, false);
		document.addEventListener("mouseup", mouseUp, false);
		document.addEventListener("mousemove", mouseMove, false);
	}

	window.addEventListener("load", onload, false);
}